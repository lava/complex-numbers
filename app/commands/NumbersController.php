<?php


namespace app\commands;

use app\models\AlgebraicComplexNumber;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

class NumbersController extends Controller
{
    public $defaultAction = 'calc';

    /**
     * Complex number simple calculator
     *
     * @param string $expression Like (-1+i4)+(43-i1.5)
     * @return void
     */
    public function actionCalc($expression)
    {
        if (empty($expression)) {
            $expression = Console::input('Enter expression, e.g. (-1+i4)+(43-i1.5):');
        }

        if (!preg_match('/(\(.*\))([-+*])(\(.*\))/', $expression, $matches)) {
            Console::error('Invalid expression');
            return ExitCode::DATAERR;
        }
        $n1 = AlgebraicComplexNumber::parse($matches[1]);
        $n2 = AlgebraicComplexNumber::parse($matches[3]);

        switch ($matches[2]) {
            case '+':
                $n1->add($n2);
                break;
            case '-':
                $n1->substract($n2);
                break;
            case '*':
                $n1->multiply($n2);
                break;
            case '+':
                $n1->divide($n2);
                break;
            default:
                Console::error('Invalid expression operator');
                return ExitCode::DATAERR;
        }
        Console::output($n1);
        return ExitCode::OK;
    }
}
