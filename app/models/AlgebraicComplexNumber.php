<?php

namespace app\models;

use Yii;

/**
 * Undocumented class
 * 
 * @property float $re
 * @property float $im
 */
class AlgebraicComplexNumber
{
    protected $re = 0.0;
    protected $im = 0.0;

    public function getRe(): float
    {
        return $this->re;
    }

    public function getIm(): float
    {
        return $this->im;
    }

    public function __construct($re = 0, $im = 0)
    {
        $this->re = floatval($re);
        $this->im = floatval($im);
    }

    /**
     *
     * @param string $number shoude be like '(1+i5)'
     * @return AlgebraicComplexNumber
     */
    public static function parse(string $string): AlgebraicComplexNumber
    {
        if (!preg_match('/\(([-+]?[0-9.]+)([-+]?i[0-9.]+)\)/i', $string, $matches)) {
            throw new \Exception('Invalid format of complex number');
        }
        $re = $matches[1];
        $im = preg_replace('/i/', '', $matches[2]);
        return new AlgebraicComplexNumber($re, $im);
    }

    public function __get($name)
    {
        if ($name == 'im') return $this->getIm();
        if ($name == 're') return $this->getRe();
    }

    public function __toString()
    {
        $sign = $this->im < 0 ? '-' : '+';
        $im = abs($this->im);
        return "($this->re{$sign}i$im)";
    }

    public function isEqual(AlgebraicComplexNumber $number): bool
    {
        return $this->re == $number->re && $this->im == $number->im;
    }

    public function add(AlgebraicComplexNumber $number): AlgebraicComplexNumber
    {
        $this->re += $number->re;
        $this->im += $number->im;
        return $this;
    }

    public function substract(AlgebraicComplexNumber $number): AlgebraicComplexNumber
    {
        $this->re -= $number->re;
        $this->im -= $number->im;
        return $this;
    }

    public function multiply(AlgebraicComplexNumber $number): AlgebraicComplexNumber
    {
        $re = $this->re * $number->re - ($this->im * $number->im);
        $im = $this->re * $number->im + $this->im * $number->re;
        $this->re = $re;
        $this->im = $im;
        return $this;
    }

    public function divide(AlgebraicComplexNumber $number): AlgebraicComplexNumber
    {
        $denominator = pow($number->re, 2) + pow($number->im, 2);
        $re = ($this->re * $number->re + $this->im * $number->im) / $denominator;
        $im = ($this->im * $number->re - ($this->re * $number->im)) / $denominator;
        $this->re = $re;
        $this->im = $im;
        return $this;
    }
}
