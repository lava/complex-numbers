<?php

namespace tests\unit\models;

use app\models\AlgebraicComplexNumber;

class AlgebraicComplexNumberTest extends \Codeception\Test\Unit
{
    public function testCreating()
    {
        $c = new AlgebraicComplexNumber();
        $this->assertEquals(0, $c->re);
        $this->assertEquals(0, $c->im);

        $numbers = [
            [1, 2],
            [5.3, -7],
            ['-77.4', 64.24]
        ];
        foreach ($numbers as $n) {
            $c = new AlgebraicComplexNumber($n[0], $n[1]);
            $this->assertEquals(floatval($n[0]), $c->re);
            $this->assertEquals(floatval($n[1]), $c->im);
        }

        $c = AlgebraicComplexNumber::parse('(-1.5+i44.2)');
        $this->assertEquals(-1.5, $c->re);
        $this->assertEquals(44.2, $c->im);
    }

    public function testConversionToString()
    {
        $c = new AlgebraicComplexNumber(4, 8);
        $this->assertEquals('(4+i8)', (string)$c);

        $c = new AlgebraicComplexNumber(-56, -8.4);
        $this->assertEquals('(-56-i8.4)', (string)$c);
    }

    public function testAddingAndSubstracting()
    {
        $c = new AlgebraicComplexNumber(44, 55);
        $c->add(new AlgebraicComplexNumber(10.2, -4.6));
        $this->assertEquals(54.2, $c->re);
        $this->assertEquals(50.4, $c->im);

        $c->substract(new AlgebraicComplexNumber('-10', 1));
        $this->assertEquals(64.2, $c->re);
        $this->assertEquals(49.4, $c->im);

        $c->add(new AlgebraicComplexNumber(1.5, 8))->substract(new AlgebraicComplexNumber(4, 3));
        $this->assertEquals(61.7, $c->re);
        $this->assertEquals(54.4, $c->im);
    }

    public function testMultiplication()
    {
        $c = new AlgebraicComplexNumber(46, 2);
        $c->multiply(new AlgebraicComplexNumber(3, 4));
        $this->assertEquals(130, $c->re);
        $this->assertEquals(190, $c->im);

        $c->multiply(new AlgebraicComplexNumber(-2.5, 3));
        $this->assertEquals(-895, $c->re);
        $this->assertEquals(-85, $c->im);
    }

    public function testDivision()
    {
        $c = new AlgebraicComplexNumber(16, -8);
        $c->divide(new AlgebraicComplexNumber(4, 2));
        $this->assertEquals(2.4, $c->re);
        $this->assertEquals(-3.2, $c->im);

        $c->divide(new AlgebraicComplexNumber(2, 2));
        $this->assertEquals(-0.2, $c->re);
        $this->assertEquals(-1.4, $c->im);
    }
}
